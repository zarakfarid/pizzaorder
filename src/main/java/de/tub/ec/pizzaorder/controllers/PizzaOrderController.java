package de.tub.ec.pizzaorder.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.tub.ec.pizzaorder.models.Order;
import de.tub.ec.pizzaorder.models.OrderItem;
import de.tub.ec.pizzaorder.models.Pizza;
import de.tub.ec.pizzaorder.models.PizzaToppings;
import de.tub.ec.pizzaorder.models.Topping;
import de.tub.ec.pizzaorder.repos.OrderRepository;
import de.tub.ec.pizzaorder.repos.PizzaRepository;
import de.tub.ec.pizzaorder.repos.PizzaToppingsRepository;
import de.tub.ec.pizzaorder.repos.ToppingRepository;
import de.tub.ec.pizzaorder.utils.CommonUtils;

// TODO: Auto-generated Javadoc
/**
 * The Class PizzaOrderController.
 */
@RestController
public class PizzaOrderController {

	/** The Constant logger. */
	private static final Logger logger = LoggerFactory.getLogger(PizzaOrderController.class);

	/** The pizza repository. */
	private PizzaRepository pizzaRepository;

	/** The topping repository. */
	private ToppingRepository toppingRepository;

	/** The pizza toppings. */
	private PizzaToppingsRepository pizzaToppings;

	/** The order repository. */
	private OrderRepository orderRepository;

	/** The dublicate toppings. */
	private Map<Long, Long> dublicateToppings = new HashMap<>();

	/** The base price. */
	private Map<String, BigDecimal> basePrice = setupBasePrice();

	/**
	 * Instantiates a new pizza order controller.
	 *
	 * @param pizzaRepository the pizza repository
	 * @param toppingRepo the topping repo
	 * @param pizzaToppings the pizza toppings
	 * @param orderRepo the order repo
	 */
	@Autowired
	public PizzaOrderController(PizzaRepository pizzaRepository, 
			ToppingRepository toppingRepo, PizzaToppingsRepository pizzaToppings, OrderRepository orderRepo) {
		this.pizzaRepository = pizzaRepository;
		this.toppingRepository = toppingRepo;
		this.pizzaToppings = pizzaToppings;
		this.orderRepository = orderRepo;
	}

	/**
	 * Setup base price.
	 *
	 * @return the map
	 */
	private Map<String, BigDecimal> setupBasePrice() {
		Map<String, BigDecimal> basePrices = new HashMap<String, BigDecimal>();     
		basePrices.put("Standard", BigDecimal.valueOf(5.00));
		basePrices.put("Large", BigDecimal.valueOf(8.50));
		return basePrices;
	}

	/**
	 * Adds the pizza.
	 *
	 * @param body the body
	 * @return the response entity
	 */
	@RequestMapping(value = "/pizza",produces = { "application/json" }, consumes = { "application/json" },method = RequestMethod.POST)
	ResponseEntity<Void> addPizza(@Valid @RequestBody Pizza body, HttpServletRequest httpRequest){
		logger.info("This is the Pizza:"+body.toString());
		if(body.getName() == null) {
			return new ResponseEntity<Void>(HttpStatus.valueOf(400));
		}
		if(body.getSize() == null) {
			body.setSize(body.getSize().STANDARD);
			body.setPrice(basePrice.get(body.getSize().STANDARD));
		}else {
			body.setPrice(basePrice.get(body.getSize().toString()));
		}
		body.setId(null);
		pizzaRepository.save(body);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.set("Location", httpRequest.getRequestURL()+"/"+body.getId());
		logger.info("This is the Pizza:"+body.toString());
		return new ResponseEntity<>(headers, HttpStatus.valueOf(201));
	}

	/**
	 * Creates the topping.
	 *
	 * @param pizzaId the pizza id
	 * @param body the body
	 * @return the response entity
	 */
	@RequestMapping(value = "/pizza/{pizzaId}/topping",produces = { "application/json" }, consumes = { "application/json" },method = RequestMethod.POST)
	ResponseEntity<Void> createTopping(@PathVariable("pizzaId") Long pizzaId,@Valid @RequestBody Topping body, HttpServletRequest httpRequest){
		PizzaToppings pizzaTopping = null;
		ResponseEntity<Void> response = null;
		body.setId(null);
		
		if(pizzaRepository.exists(pizzaId)) {
			logger.info("This is the Toppings:"+body.toString());
			if(body.getPrice() == null || body.getPrice().compareTo(BigDecimal.ZERO)<0 || body.getName() == null) {
				response = new ResponseEntity<Void>(HttpStatus.valueOf(400));
			}
			toppingRepository.save(body);
			if(pizzaToppings.exists(pizzaId)) {
				pizzaTopping = pizzaToppings.findOne(pizzaId);
				pizzaTopping.getToppings().add(body.getId());
				pizzaToppings.save(pizzaTopping);
				///////////////////////////////////////////////
				addPizzaPrice(pizzaId, body.getPrice());
				///////////////////////////////////////////////
				CommonUtils.setDublicateToppings(body.getId(), dublicateToppings);
				///////////////////////////////////////////////
				logger.info("Pizza Toppings Bitch:"+pizzaTopping.getToppings().toString());
			}else {
				///////////////////////////////////////////////
				PizzaToppings toppings = new PizzaToppings();
				toppings.setId(pizzaId);
				List<Long> topingIds = new ArrayList<>();
				topingIds.add(body.getId());
				toppings.setToppings(topingIds);
				pizzaToppings.save(toppings);
				///////////////////////////////////////////////
				addPizzaPrice(pizzaId, body.getPrice());
				///////////////////////////////////////////////
				CommonUtils.setDublicateToppings(body.getId(), dublicateToppings);
				///////////////////////////////////////////////
				logger.info("Pizza Toppings Saved:"+toppings.getToppings().toString());
			}
			MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
			headers.set("Location", httpRequest.getRequestURL()+"/"+body.getId());
			response = new ResponseEntity<Void>(headers, HttpStatus.valueOf(201));
		}else {
			response = new  ResponseEntity<Void>(HttpStatus.valueOf(404));
		}
		return response;
	}

	/**
	 * Adds the pizza price.
	 *
	 * @param pizzaId the pizza id
	 * @param price the price
	 */
	private void addPizzaPrice(Long pizzaId, BigDecimal price) {
		Pizza pizza= pizzaRepository.findOne(pizzaId);
		BigDecimal newPrice = pizza.getPrice().add(price);
		pizza.setPrice(newPrice);
		pizzaRepository.save(pizza);
	}

	/**
	 * Delete pizza.
	 *
	 * @param pizzaId the pizza id
	 * @return the response entity
	 */
	@RequestMapping(value = "/pizza/{pizzaId}", produces = { "application/json" }, method = RequestMethod.DELETE)
	ResponseEntity<Void> deletePizza(@PathVariable("pizzaId") Long pizzaId){
		Pizza pizza = null;
		ResponseEntity<Void> response = null;

		if(pizzaRepository.exists(pizzaId)) {
			if(pizzaId == null) {
				return new ResponseEntity<Void>(HttpStatus.valueOf(400));
			}
			logger.info("Pizza Deleted:"+pizzaId);
			pizzaRepository.delete(pizzaId);
			if(pizzaToppings.exists(pizzaId)) {
				pizzaToppings.delete(pizzaId);
			}
			response = new ResponseEntity<Void>(HttpStatus.valueOf(204));
		}else {
			logger.info("Pizza Not Found Id:"+pizzaId);
			response = new ResponseEntity<Void>(HttpStatus.valueOf(404));
		}
		return response;
	}

	/**
	 * Delete topping by id.
	 *
	 * @param pizzaId the pizza id
	 * @param toppingId the topping id
	 * @return the response entity
	 */
	@RequestMapping(value = "/pizza/{pizzaId}/topping/{toppingId}",produces = { "application/json" }, method = RequestMethod.DELETE)
	ResponseEntity<Void> deleteToppingById(@PathVariable("pizzaId") Long pizzaId, @PathVariable("toppingId") Long toppingId){
		ResponseEntity<Void> response = null;

		if(toppingRepository.exists(toppingId) && pizzaRepository.exists(pizzaId)) {
			if(pizzaToppings.exists(pizzaId)) {
				PizzaToppings pizzaTopping = pizzaToppings.findOne(pizzaId);
				boolean onPizza = false;
				List<Long> alltoppings = new ArrayList<>();
				for(Long i : pizzaTopping.getToppings()) {
					if(i.equals(toppingId)) {
						onPizza = true;
					}else {
						alltoppings.add(i);
					}
				}
				if(onPizza) {
					subtractPizzaPrice(pizzaId, toppingId);
					if(pizzaTopping.getToppings().size() == 1) {
						pizzaToppings.delete(pizzaId);
					}else {
						//Removed from list
						pizzaTopping.setToppings(alltoppings);
						pizzaToppings.save(pizzaTopping);
					}
					if(dublicateToppings.get(toppingId)>1) {
						dublicateToppings.put(toppingId, dublicateToppings.get(toppingId)-1);
					}else {
						toppingRepository.delete(toppingId);
					}
					response = new ResponseEntity<Void>(HttpStatus.OK);
				}else {
					response = new ResponseEntity<Void>(HttpStatus.valueOf(400));
				}
			}else {
				response = new ResponseEntity<Void>(HttpStatus.valueOf(400));
			}
		}else {
			response = new ResponseEntity<Void>(HttpStatus.valueOf(404));
		}
		return response ;
	}

	/**
	 * Subtract pizza price.
	 *
	 * @param pizzaId the pizza id
	 * @param toppingId the topping id
	 */
	private void subtractPizzaPrice(Long pizzaId, Long toppingId) {
		Pizza pizza= pizzaRepository.findOne(pizzaId);
		BigDecimal newPrice = pizza.getPrice().subtract(toppingRepository.findOne(toppingId).getPrice());
		pizza.setPrice(newPrice);
		pizzaRepository.save(pizza);
	}

	/**
	 * Gets the pizza by id.
	 *
	 * @param pizzaId the pizza id
	 * @return the pizza by id
	 */
	@RequestMapping(value = "/pizza/{pizzaId}",produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<Pizza> getPizzaById(@PathVariable("pizzaId") Long pizzaId){
		Pizza pizza = null;
		ResponseEntity<Pizza> response = null;

		if(pizzaRepository.exists(pizzaId)) {
			pizza = pizzaRepository.findOne(pizzaId);
			logger.info("Pizza Found:"+pizza);
			response = new ResponseEntity<Pizza>(pizza, HttpStatus.OK);
		}else {
			logger.info("Pizza Not Found Id:"+pizzaId);
			response = new ResponseEntity<Pizza>(pizza, HttpStatus.valueOf(404));
		}
		return response;
	}

	/**
	 * Gets the pizzas.
	 *
	 * @return the pizzas
	 */
	@RequestMapping(value = "/pizza",produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<List<Integer>> getPizzas(){
		ResponseEntity<List<Integer>> response = null;
		List<Integer> pizzaIds = null;

		long count = pizzaRepository.count();
		if(count > 0) {
			pizzaIds = new ArrayList<>();
			Iterable<Pizza> pizzas = pizzaRepository.findAll();
			if(pizzas!=null) {
				Iterator<Pizza> pizzaIterator = pizzas.iterator();
				if(pizzaIterator!=null) {
					while (pizzaIterator.hasNext()){
						pizzaIds.add(pizzaIterator.next().getId().intValue());
					}	
				}
			}
			response = new ResponseEntity<List<Integer>>(pizzaIds, HttpStatus.OK);
		}else {
			response = new ResponseEntity<List<Integer>>(pizzaIds, HttpStatus.valueOf(404));
		}
		return response ;
	}

	/**
	 * Gets the topping by id.
	 *
	 * @param pizzaId the pizza id
	 * @param toppingId the topping id
	 * @return the topping by id
	 */
	@RequestMapping(value = "/pizza/{pizzaId}/topping/{toppingId}",produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<Topping> getToppingById(@PathVariable("pizzaId") Long pizzaId, @PathVariable("toppingId") Long toppingId){
		ResponseEntity<Topping> response = null;
		Topping topping = null;

		if(toppingRepository.exists(toppingId) && pizzaRepository.exists(pizzaId)) {
			if(pizzaToppings.exists(pizzaId)) {
				PizzaToppings pizzaTopping = pizzaToppings.findOne(pizzaId);
				boolean onPizza = false;
				for(Long i : pizzaTopping.getToppings()) {
					if(i.equals(toppingId)) {
						onPizza = true;
					}
				}
				if(onPizza) {
					topping  = toppingRepository.findOne(toppingId);
					response = new ResponseEntity<Topping>(topping, HttpStatus.OK);
				}else {
					response = new ResponseEntity<Topping>(topping, HttpStatus.valueOf(400));
				}
			}else {
				response = new ResponseEntity<Topping>(topping, HttpStatus.valueOf(400));
			}
		}else {
			response = new ResponseEntity<Topping>(topping, HttpStatus.valueOf(404));
		}
		return response ;
	}


	/**
	 * List toppings.
	 *
	 * @param pizzaId the pizza id
	 * @return the response entity
	 */
	@RequestMapping(value = "/pizza/{pizzaId}/topping",produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<List<Long>> listToppings(@PathVariable("pizzaId") Long pizzaId){
		ResponseEntity<List<Long>> response = null;
		List<Long> toppings = null;
		if(pizzaRepository.exists(pizzaId)) {
			if(pizzaToppings.exists(pizzaId)) {
				PizzaToppings pizzatoppings = pizzaToppings.findOne(pizzaId);
				if(pizzatoppings!=null && pizzatoppings.getToppings()!=null && pizzatoppings.getToppings().size()>0) {
					response = new ResponseEntity<List<Long>>(pizzatoppings.getToppings(), HttpStatus.OK);
				}
			}else {
				response = new ResponseEntity<List<Long>>(toppings, HttpStatus.valueOf(400));
			}
		}else {
			response = new ResponseEntity<List<Long>>(toppings, HttpStatus.valueOf(404));
		}
		return response ;
	}


	/**
	 * Update pizza.
	 *
	 * @param body the body
	 * @param pizzaId the pizza id
	 * @return the response entity
	 */
	@RequestMapping(value = "/pizza/{pizzaId}",produces = { "application/json" }, consumes = { "application/json" },method = RequestMethod.PUT)
	ResponseEntity<Void> updatePizza(@Valid @RequestBody Pizza body, @PathVariable("pizzaId") Long pizzaId){
		Pizza pizza = null;
		ResponseEntity<Void> response = null;

		if(pizzaId != null && pizzaRepository.exists(pizzaId)) {
			if(body.getPrice() != null &&  body.getPrice().compareTo(BigDecimal.ZERO)<0) {
				return new ResponseEntity<Void>(HttpStatus.valueOf(400));
			}
			logger.info("Pizza Found:"+pizzaId);
			body.setId(pizzaId);
			updatePizza(pizzaId, body);
			response = new ResponseEntity<Void>(HttpStatus.valueOf(204));
		}else {
			logger.info("Pizza Not Found Id:"+pizzaId);
			response = new ResponseEntity<Void>(HttpStatus.valueOf(404));
		}
		return response;
	}

	private void updatePizza(Long pizzaId, Pizza body) {
		Pizza oldPizza = pizzaRepository.findOne(pizzaId);
		if(body.getSize()!=null) {
			oldPizza.setSize(body.getSize());
			oldPizza.setPrice(basePrice.get(body.getSize().toString()));
		}if(body.getName()!=null) {
			oldPizza.setName(body.getName());
		}
		pizzaRepository.save(oldPizza);
	}

	/**
	 * Creates the order.
	 *
	 * @param body the body
	 * @return the response entity
	 */
	@RequestMapping(value = "/order", produces = { "application/json" }, consumes = { "application/json" }, method = RequestMethod.POST)
	ResponseEntity<Void> createOrder( @Valid @RequestBody Order body, HttpServletRequest httpRequest){
		if(checkValidateOrder(body)) {
			return new ResponseEntity<Void>(HttpStatus.valueOf(400));
		}
		body.setId(null);
		orderRepository.save(body);
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.set("Location", httpRequest.getRequestURL()+"/"+body.getId());
		return new ResponseEntity<>(headers, HttpStatus.valueOf(201));
	}

	/**
	 * Check validate order.
	 *
	 * @param body the body
	 * @return true, if successful
	 */
	private boolean checkValidateOrder(Order body) {
		if(body.getRecipient() == null || body.getOrderItems() == null) {
			return true;
		}
		float total = 0;
		if(body.getOrderItems() != null) {
			for(OrderItem item : body.getOrderItems()) {
				if(item.getPizzaId() == null || !pizzaRepository.exists(item.getPizzaId()) || item.getQuantity() == null) {
					return true;
				}else{
					Pizza pizza = pizzaRepository.findOne(item.getPizzaId());
					logger.info("Price is:"+pizza.getPrice().floatValue());
					total = total + pizza.getPrice().floatValue();
				}
			}
		}
		body.setTotalPrice(new BigDecimal(total));
		logger.info("Order Price is:"+body.getTotalPrice());
		return false;
	}

	/**
	 * Delete order.
	 *
	 * @param orderId the order id
	 * @return the response entity
	 */
	@RequestMapping(value = "/order/{orderId}",produces = { "application/json" }, method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteOrder(@PathVariable("orderId") Long orderId) {
		ResponseEntity<Void> response = null;

		if(orderRepository.exists(orderId)) {
			if(orderId == null) {
				return new ResponseEntity<Void>(HttpStatus.valueOf(400));
			}
			logger.info("Pizza Deleted:"+orderId);
			orderRepository.delete(orderId);
			response = new ResponseEntity<Void>(HttpStatus.valueOf(204));
		}else {
			logger.info("Pizza Not Found Id:"+orderId);
			response = new ResponseEntity<Void>(HttpStatus.valueOf(404));
		}
		return response;
	}

	/**
	 * Gets the order by id.
	 *
	 * @param orderId the order id
	 * @return the order by id
	 */
	@RequestMapping(value = "/order/{orderId}", produces = { "application/json" }, method = RequestMethod.GET)
	ResponseEntity<Order> getOrderById(@PathVariable("orderId") Long orderId) {
		Order order = null;
		ResponseEntity<Order> response = null;

		if(orderRepository.exists(orderId)) {
			order = orderRepository.findOne(orderId);
			logger.info("Order Found:"+order);
			response = new ResponseEntity<Order>(order, HttpStatus.OK);
		}else {
			logger.info("Pizza Not Found Id:"+orderId);
			response = new ResponseEntity<Order>(order, HttpStatus.valueOf(404));
		}
		return response;		
	}

	/**
	 * Gets the orders.
	 *
	 * @return the orders
	 */
	@RequestMapping(value = "/order",produces = { "application/json" }, method = RequestMethod.GET)
	public ResponseEntity<List<Integer>> getOrders() {
		ResponseEntity<List<Integer>> response = null;
		List<Integer> orderIds = null;

		long count = orderRepository.count();
		if(count > 0) {
			orderIds = new ArrayList<>();
			Iterable<Order> orders = orderRepository.findAll();
			if(orders!=null) {
				Iterator<Order> pizzaIterator = orders.iterator();
				if(pizzaIterator!=null) {
					while (pizzaIterator.hasNext()){
						orderIds.add(pizzaIterator.next().getId().intValue());
					}	
				}
			}
			response = new ResponseEntity<List<Integer>>(orderIds, HttpStatus.OK);
		}else {
			response = new ResponseEntity<List<Integer>>(orderIds, HttpStatus.valueOf(404));
		}
		return response ;
	}

}