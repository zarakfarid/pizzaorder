package de.tub.ec.pizzaorder.models;

import java.util.Objects;

import javax.persistence.Embeddable;

// TODO: Auto-generated Javadoc
/**
 * OrderItem.
 */
@Embeddable
public class OrderItem {

	/** The pizza id. */
	private Long pizzaId = null;

	/** The quantity. */
	private Long quantity = null;

	/**
	 * Pizza id.
	 *
	 * @param pizzaId the pizza id
	 * @return the order item
	 */
	public OrderItem pizzaId(Long pizzaId) {
		this.pizzaId = pizzaId;
		return this;
	}
	
	/**
	 * Get pizzaId.
	 *
	 * @return pizzaId
	 */

	public Long getPizzaId() {
		return pizzaId;
	}

	/**
	 * Sets the pizza id.
	 *
	 * @param pizzaId the new pizza id
	 */
	public void setPizzaId(Long pizzaId) {
		this.pizzaId = pizzaId;
	}

	/**
	 * Quantity.
	 *
	 * @param quantity the quantity
	 * @return the order item
	 */
	public OrderItem quantity(Long quantity) {
		this.quantity = quantity;
		return this;
	}

	/**
	 * Get quantity.
	 *
	 * @return quantity
	 */

	public Long getQuantity() {
		return quantity;
	}

	/**
	 * Sets the quantity.
	 *
	 * @param quantity the new quantity
	 */
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		OrderItem orderItem = (OrderItem) o;
		return Objects.equals(this.pizzaId, orderItem.pizzaId) &&
				Objects.equals(this.quantity, orderItem.quantity);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(pizzaId, quantity);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class OrderItem {\n");
		
		sb.append("    pizzaId: ").append(toIndentedString(pizzaId)).append("\n");
		sb.append("    quantity: ").append(toIndentedString(quantity)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 *
	 * @param o the o
	 * @return the string
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

