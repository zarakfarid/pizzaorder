package de.tub.ec.pizzaorder.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

// TODO: Auto-generated Javadoc
/**
 * Topping.
 */
@Entity
public class Topping implements Serializable  {
	
	/** The id. */
	@Id
	@SequenceGenerator(name = "topping_id_seq", sequenceName = "topping_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "topping_id_seq")
	private Long id = null;

	/** The name. */
	private String name = null;

	/** The price. */
	private BigDecimal price = null;

	/**
	 * Id.
	 *
	 * @param id the id
	 * @return the topping
	 */
	public Topping id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id.
	 *
	 * @return id
	 */

	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Name.
	 *
	 * @param name the name
	 * @return the topping
	 */
	public Topping name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name.
	 *
	 * @return name
	 */

	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Price.
	 *
	 * @param price the price
	 * @return the topping
	 */
	public Topping price(BigDecimal price) {
		this.price = price;
		return this;
	}

	/**
	 * Get price.
	 *
	 * @return price
	 */

	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Topping topping = (Topping) o;
		return Objects.equals(this.id, topping.id) &&
				Objects.equals(this.name, topping.name) &&
				Objects.equals(this.price, topping.price);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(id, name, price);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Topping {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    price: ").append(toIndentedString(price)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 *
	 * @param o the o
	 * @return the string
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

