package de.tub.ec.pizzaorder.models;

import java.util.Objects;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

// TODO: Auto-generated Javadoc
/**
 * Pizza.
 */
@Entity
public class Pizza  implements Serializable {
	
	/** The id. */
	@Id
	@SequenceGenerator(name = "pizza_id_seq", sequenceName = "pizza_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pizza_id_seq")
	private Long id;

	/** The name. */
	private String name = null;

	/**
	 * Size.
	 */
	public enum SizeEnum {
		
		/** The standard. */
		STANDARD("Standard"),

		/** The large. */
		LARGE("Large");

		/** The value. */
		private String value;

		/**
		 * Instantiates a new size enum.
		 *
		 * @param value the value
		 */
		SizeEnum(String value) {
			this.value = value;
		}

		/* (non-Javadoc)
		 * @see java.lang.Enum#toString()
		 */
		@Override
		public String toString() {
			return String.valueOf(value);
		}

		/**
		 * From value.
		 *
		 * @param text the text
		 * @return the size enum
		 */
		public static SizeEnum fromValue(String text) {
			for (SizeEnum b : SizeEnum.values()) {
				if (String.valueOf(b.value).equals(text)) {
					return b;
				}
			}
			return null;
		}
	}


	/** The size. */
	private SizeEnum size = null;

	/** The price. */
	private BigDecimal price = null;

	/**
	 * Id.
	 *
	 * @param id the id
	 * @return the pizza
	 */
	public Pizza id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id.
	 *
	 * @return id
	 */

	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Name.
	 *
	 * @param name the name
	 * @return the pizza
	 */
	public Pizza name(String name) {
		this.name = name;
		return this;
	}

	/**
	 * Get name.
	 *
	 * @return name
	 */

	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Size.
	 *
	 * @param size the size
	 * @return the pizza
	 */
	public Pizza size(SizeEnum size) {
		this.size = size;
		return this;
	}

	/**
	 * Size.
	 *
	 * @return size
	 */

	public SizeEnum getSize() {
		return size;
	}

	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize(SizeEnum size) {
		this.size = size;
	}

	/**
	 * Price.
	 *
	 * @param price the price
	 * @return the pizza
	 */
	public Pizza price(BigDecimal price) {
		this.price = price;
		return this;
	}

	/**
	 * Price including toppings.
	 * @return price
	 **/

	public BigDecimal getPrice() {
		return price;
	}

	/**
	 * Sets the price.
	 *
	 * @param price the new price
	 */
	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Pizza pizza = (Pizza) o;
		return Objects.equals(this.id, pizza.id) &&
				Objects.equals(this.name, pizza.name) &&
				Objects.equals(this.size, pizza.size) &&
				Objects.equals(this.price, pizza.price);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(id, name, size, price);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Pizza {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    name: ").append(toIndentedString(name)).append("\n");
		sb.append("    size: ").append(toIndentedString(size)).append("\n");
		sb.append("    price: ").append(toIndentedString(price)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 *
	 * @param o the o
	 * @return the string
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

