package de.tub.ec.pizzaorder.models;

import java.util.List;
import java.util.Objects;

import java.io.Serializable;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;

// TODO: Auto-generated Javadoc
/**
 * PizzaTopping.
 */
@Entity
public class PizzaToppings implements Serializable  {
  
  /** The id. */
  @Id
  private Long id = null;

  /** The toppings. */
  @ElementCollection(targetClass=Long.class)
  private List<Long> toppings = null;

  /**
   * Id.
   *
   * @param id the id
   * @return the pizza toppings
   */
  public PizzaToppings id(Long id) {
    this.id = id;
    return this;
  }

   /**
    * Get id.
    *
    * @return id
    */

  public Long getId() {
    return id;
  }

  /**
   * Sets the id.
   *
   * @param id the new id
   */
  public void setId(Long id) {
    this.id = id;
  }

  /**
   * Toppings.
   *
   * @param toppings the toppings
   * @return the pizza toppings
   */
  public PizzaToppings toppings(List<Long> toppings) {
    this.toppings = toppings;
    return this;
  }

   /**
    * Get topping.
    *
    * @return toppings
    */

  public List<Long> getToppings() {
    return toppings;
  }

  /**
   * Sets the toppings.
   *
   * @param toppings the new toppings
   */
  public void setToppings(List<Long> toppings) {
    this.toppings = toppings;
  }


  /* (non-Javadoc)
   * @see java.lang.Object#equals(java.lang.Object)
   */
  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PizzaToppings pizzaToppings = (PizzaToppings) o;
    return Objects.equals(this.id, pizzaToppings.id) &&
        Objects.equals(this.toppings, pizzaToppings.toppings);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#hashCode()
   */
  @Override
  public int hashCode() {
    return Objects.hash(id, toppings);
  }

  /* (non-Javadoc)
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Topping {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    toppings: ").append(toIndentedString(toppings)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   *
   * @param o the o
   * @return the string
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

