package de.tub.ec.pizzaorder.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

// TODO: Auto-generated Javadoc
/**
 * Order.
 */
@Entity
@Table(name = "order_table")
public class Order  implements Serializable {

	/** The id. */
	@Id
	@SequenceGenerator(name = "order_id_seq", sequenceName = "order_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_id_seq")
	private Long id = null;

	/** The order items. */
	@ElementCollection
	@CollectionTable(name = "OrderItem", joinColumns = {@JoinColumn(name="order_id")})	
	private List<OrderItem> orderItems = new ArrayList<>();
	
	/** The total price. */
	private BigDecimal totalPrice = null;

	/** The recipient. */
	private String recipient = null;

	/**
	 * Id.
	 *
	 * @param id the id
	 * @return the order
	 */
	public Order id(Long id) {
		this.id = id;
		return this;
	}

	/**
	 * Get id.
	 *
	 * @return id
	 */

	public Long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Order items.
	 *
	 * @param orderItems the order items
	 * @return the order
	 */
	public Order orderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
		return this;
	}

	/**
	 * Adds the order items item.
	 *
	 * @param orderItemsItem the order items item
	 * @return the order
	 */
	public Order addOrderItemsItem(OrderItem orderItemsItem) {
		this.orderItems.add(orderItemsItem);
		return this;
	}

	/**
	 * Get orderItems.
	 *
	 * @return orderItems
	 */

	public List<OrderItem> getOrderItems() {
		return orderItems;
	}

	/**
	 * Sets the order items.
	 *
	 * @param orderItems the new order items
	 */
	public void setOrderItems(List<OrderItem> orderItems) {
		this.orderItems = orderItems;
	}

	/**
	 * Total price.
	 *
	 * @param totalPrice the total price
	 * @return the order
	 */
	public Order totalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
		return this;
	}

	/**
	 * Get totalPrice.
	 *
	 * @return totalPrice
	 */

	public BigDecimal getTotalPrice() {
		return totalPrice;
	}

	/**
	 * Sets the total price.
	 *
	 * @param totalPrice the new total price
	 */
	public void setTotalPrice(BigDecimal totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * Recipient.
	 *
	 * @param recipient the recipient
	 * @return the order
	 */
	public Order recipient(String recipient) {
		this.recipient = recipient;
		return this;
	}

	/**
	 * Get recipient.
	 *
	 * @return recipient
	 */

	public String getRecipient() {
		return recipient;
	}

	/**
	 * Sets the recipient.
	 *
	 * @param recipient the new recipient
	 */
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(java.lang.Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Order order = (Order) o;
		return Objects.equals(this.id, order.id) &&
				Objects.equals(this.orderItems, order.orderItems) &&
				Objects.equals(this.totalPrice, order.totalPrice) &&
				Objects.equals(this.recipient, order.recipient);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return Objects.hash(id, orderItems, totalPrice, recipient);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class Order {\n");

		sb.append("    id: ").append(toIndentedString(id)).append("\n");
		sb.append("    orderItems: ").append(toIndentedString(orderItems)).append("\n");
		sb.append("    totalPrice: ").append(toIndentedString(totalPrice)).append("\n");
		sb.append("    recipient: ").append(toIndentedString(recipient)).append("\n");
		sb.append("}");
		return sb.toString();
	}

	/**
	 * Convert the given object to string with each line indented by 4 spaces
	 * (except the first line).
	 *
	 * @param o the o
	 * @return the string
	 */
	private String toIndentedString(java.lang.Object o) {
		if (o == null) {
			return "null";
		}
		return o.toString().replace("\n", "\n    ");
	}
}

