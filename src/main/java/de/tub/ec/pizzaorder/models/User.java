package de.tub.ec.pizzaorder.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

// TODO: Auto-generated Javadoc
/**
 * The Class User.
 */
@Entity
public class User implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -6640481949420444264L;
	
	/** The name. */
	@Id
	String name;
	

	/**
	 * Instantiates a new user.
	 */
	protected User() {
		//empty constructor required by JPA
	}

	/**
	 * Instantiates a new user.
	 *
	 * @param name the name
	 */
	public User(String name) {
		this.name = name;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}
}