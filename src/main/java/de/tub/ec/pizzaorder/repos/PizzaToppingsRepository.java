package de.tub.ec.pizzaorder.repos;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import de.tub.ec.pizzaorder.models.PizzaToppings;

/**
 * The Interface PizzaToppingsRepository.
 */
public interface PizzaToppingsRepository extends CrudRepository<PizzaToppings, Long> {
//    Optional<PizzaToppings> findByName(String name);
}
