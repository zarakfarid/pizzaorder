package de.tub.ec.pizzaorder.repos;

import org.springframework.data.repository.CrudRepository;

import de.tub.ec.pizzaorder.models.User;

import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Interface UserRepository.
 */
public interface UserRepository extends CrudRepository<User, String> {
    
    /**
     * Find by name.
     *
     * @param name the name
     * @return the optional
     */
    Optional<User> findByName(String name);
}
