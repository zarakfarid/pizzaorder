package de.tub.ec.pizzaorder.repos;

import org.springframework.data.repository.CrudRepository;

import de.tub.ec.pizzaorder.models.Topping;

import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Interface ToppingRepository.
 */
public interface ToppingRepository extends CrudRepository<Topping, Long> {
    
    /**
     * Find by name.
     *
     * @param name the name
     * @return the optional
     */
    Optional<Topping> findByName(String name);
}
