package de.tub.ec.pizzaorder.repos;

import org.springframework.data.repository.CrudRepository;

import de.tub.ec.pizzaorder.models.Pizza;

import java.util.Optional;

// TODO: Auto-generated Javadoc
/**
 * The Interface PizzaRepository.
 */
public interface PizzaRepository extends CrudRepository<Pizza, Long> {
    
    /**
     * Find by name.
     *
     * @param name the name
     * @return the optional
     */
    Optional<Pizza> findByName(String name);
}
