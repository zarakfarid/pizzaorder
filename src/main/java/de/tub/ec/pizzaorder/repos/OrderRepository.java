package de.tub.ec.pizzaorder.repos;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import de.tub.ec.pizzaorder.models.Order;

/**
 * The Interface OrderRepository.
 */
public interface OrderRepository extends CrudRepository<Order, Long> {
//    Optional<Order> findByName(String name);
}
