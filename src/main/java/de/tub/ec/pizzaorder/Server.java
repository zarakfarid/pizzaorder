package de.tub.ec.pizzaorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

// TODO: Auto-generated Javadoc
/**
 * The Class Server.
 */
@SpringBootApplication
@ComponentScan("de.tub.ec.pizzaorder")
public class Server {

    /**
     * The main method.
     *
     * @param args the arguments
     */
    public static void main(String[] args) {
        
        SpringApplication.run(Server.class, args);
    }

}
