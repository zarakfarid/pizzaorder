package de.tub.ec.pizzaorder.utils;

import java.util.Map;

// TODO: Auto-generated Javadoc
/**
 * The Class CommonUtils.
 */
public class CommonUtils {

	/**
	 * Sets the dublicate toppings.
	 *
	 * @param id the id
	 * @param dublicateToppings the dublicate toppings
	 */
	public static void setDublicateToppings(Long id, Map<Long, Long> dublicateToppings) {
		if(dublicateToppings.containsKey(id)) {
			dublicateToppings.put(id, dublicateToppings.get(id)+1);
		}else {
			dublicateToppings.put(id, (long) 1);
		}
	}
}
